/**
* @file 	main.cpp
* @brief 	Funcao principal do programa
* @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
* @since 	26/06/2017
* @date 	27/06/2017
*/

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <cstdlib>
#include <ctime>

#include "rodolpho.h"

using namespace edb1;

/**
 * @brief 	Função principal.
 * @param	argc 	Quantidade de argumentos passados em linha de comando
 * @param	*argv[] Vetor com os argumentos
 * @return	Retorno da função
 */
int main(int argc, char* argv[]) {	
    cout << "\t:::::\tTeste da lista\t:::::" << endl;
    Lista<int> *lista = new Lista<int>();
    lista->Insere(50);
    lista->Insere(60);
    lista->Insere(70);
    lista->Exibe();
    lista->RemovePos(0);
    lista->RemovePos(0);
    lista->RemovePos(0);
    lista->RemovePos(0);	//Erro
    
    //Classe pilha
    cout << "\t:::::\tTeste de pilha\t:::::" << endl;
    Pilha<int> pilha(3);
    pilha.Push(50);
    pilha.Push(60);
    pilha.Push(70);
    pilha.Push(100); //Erro
    while(!pilha.Vazio())
        cout << pilha.Pop() << endl;
    pilha.Pop(); //Erro

    //Classe fila
    cout << "\t:::::\tTeste de fila\t:::::" << endl;
    Fila<int> fila(3);
    fila.Push(30);
    fila.Push(40);
    fila.Push(55);
    fila.Push(100); //Erro
    while(!fila.Vazio())
        cout << fila.Pop() << endl;
    fila.Pop(); //Erro

    //Buscas
    cout << "\t:::::\tTeste de buscas\t:::::" << endl;
    int V[40];
    for(int i = 0; i < 40; i++)
        V[i] = i + 1;
    cout << "\t:::::\tBusca sequencial\t:::::" << endl;
    cout <<  sequencialSearchI(V, 0, 100, 10) << endl;
    cout << "Busca sequencial recursiva" << endl;
    cout <<  sequencialSearchR(V, 0, 100, 15) << endl;
    cout << "Busca sequencial iterativa" << endl;
    cout <<  sequencialSearchI(V, 0, 100, 105) << endl;  //Erro
    cout << "Busca sequencial recursiva" << endl;
    cout <<  sequencialSearchR(V, 0, 100, 10000) << endl;  //Erro

    cout << "\t:::::\tBusca binária\t:::::" << endl;
    cout <<  binarySearchI(V, 0, 100, 10) << endl;
    cout << "Busca binária recursiva" << endl;
    cout <<  binarySearchR(V, 0, 100, 15) << endl;
    cout << "Busca binária iterativa" << endl;
    cout <<  binarySearchI(V, 0, 100, 105) << endl;  //Erro
    cout << "Busca binária recursiva" << endl;
    cout <<  binarySearchR(V, 0, 100, 10000) << endl;  //Erro
    
    cout << "\t:::::\tBusca ternária\t:::::" << endl;
    cout << ternarySearchI(V, 0, 100, 10) << endl;
    cout << "Busca ternária recursiva" << endl;
    cout << ternarySearchR(V, 0, 100, 15) << endl;
    cout << "Busca ternária iterativa" << endl;
    cout << ternarySearchI(V, 0, 100, 105) << endl;  //Erro
    cout << "Busca ternária recursiva" << endl;
    cout << ternarySearchR(V, 0, 100, 10000) << endl;  //Erro

    //Ordenações

    srand(time(NULL));
    double *X = new double[10];    
    cout << "\t:::::\tInsertion sort\t:::::" << endl;
    for(int i = 0; i < 10; i++)
        X[i] = rand() % 100;
    //Exibe o vetor
    cout << "Vetor desordenado" << endl;
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Ordena o vetor
    cout << "Ordena vazio" << endl;
    insertionSort(X, 0);          //Erro
    cout << "Ordena com insertion sort" << endl;
    insertionSort(X, 10);
    //Exibe o vetor
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Inicializa aleatoriamente
    cout << "\t:::::\tSelection Sort\t:::::" << endl;
    for(int i = 0; i < 10; i++)
        X[i] = rand() % 100;
    //Exibe o vetor
    cout << "Vetor desordenado" << endl;
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Ordena o vetor
    cout << "Ordena vazio" << endl;
    selectionSort(X, 0);          //Erro
    cout << "Ordena com selection sort" << endl;
    selectionSort(X, 10);
    //Exibe o vetor
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Inicializa aleatoriamente
    cout << "\t:::::\tMerge sort\t:::::" << endl;
    for(int i = 0; i < 10; i++)
        X[i] = rand() % 100;
    //Exibe o vetor
    cout << "Vetor desordenado" << endl;
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Ordena o vetor
    cout << "Ordena vazio" << endl;
    mergeSort(X, 1, 0);          //Erro
    cout << "Ordena com merge sort" << endl;
    mergeSort(X, 0, 9);
    //Exibe o vetor
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Inicializa aleatoriamente
    cout << "\t:::::\tQuick Sort\t:::::" << endl;
    for(int i = 0; i < 10; i++)
        X[i] = rand() % 100;
    //Exibe o vetor
    cout << "Vetor desordenado" << endl;
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Ordena o vetor
    cout << "Ordena vazio" << endl;
    quickSort(X, 1, 0);          //Erro
    cout << "Ordena com quick sort" << endl;
    quickSort(X, 0, 9);
    //Exibe o vetor
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Inicializa aleatoriamente
    cout << "\t:::::\tBubble Sort\t:::::" << endl;
    for(int i = 0; i < 10; i++)
        X[i] = rand() % 100;
    //Exibe o vetor
    cout << "Vetor desordenado" << endl;
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;

    //Ordena o vetor
    cout << "Ordena vazio" << endl;
    bubbleSort(X, 0);          //Erro
    cout << "Ordena com bubble sort" << endl;
    bubbleSort(X, 10);
    //Exibe o vetor
    for(int i = 0; i < 10; i++)
        cout << X[i] << "  ";
    cout << endl;


	return 0;
}