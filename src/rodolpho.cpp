/**
* @file 	rodolpho.cpp
* @brief	Arquivo de corpo com a inclusão da biblioteca rodolpho 
* @author	Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    20/06/2017
* @date 	20/06/2017
* @sa 		rodolpho.h
*/

#include "rodolpho.h"
