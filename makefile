#Makefile for "Laboratório 10" C++ application
#Created by Rodolpho Erick 27/06/2017

# Variaveis para os subdiretorios
INC_DIR = ./include
LIB_DIR = ./lib
SRC_DIR = ./src
OBJ_DIR = ./build
DOC_DIR = ./doc

# Compilador
CC = g++

# Opcoes de compilacao
CFLAGS 	= -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)
ARCHIVE = ar

#SO Linux
linux: rodolpho.a rodolpho.so prog_estatico prog_dinamico

#SO Windows
windows: rodolpho.lib rodolpho.dll prog_estatico.exe prog_dinamico.exe


#LINUX
# Alvo (target) para a construcao da biblioteca estática rodolpho.a
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.a: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/crash.h $(INC_DIR)/stack.h $(INC_DIR)/queue.h $(INC_DIR)/list.h $(INC_DIR)/search.h $(INC_DIR)/sort.h $(INC_DIR)/rodolpho.h
	$(CC) $(CFLAGS) -c $(SRC_DIR)/rodolpho.cpp -o $(OBJ_DIR)/rodolpho.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o
	@echo "+++ [Biblioteca estática criada em $(LIB_DIR)/$@] +++"
	@echo "============="

# Alvo (target) para a construcao da biblioteca dinâmica rodolpho.a
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.so: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/crash.h $(INC_DIR)/stack.h $(INC_DIR)/queue.h $(INC_DIR)/list.h $(INC_DIR)/search.h $(INC_DIR)/sort.h $(INC_DIR)/rodolpho.h
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/rodolpho.cpp -o $(OBJ_DIR)/rodolpho.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o
	@echo "+++ [Biblioteca dinâmica criada em $(LIB_DIR)/$@] +++"
	@echo "============="

prog_estatico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.a -o $(OBJ_DIR)/$@

prog_dinamico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.so -o $(OBJ_DIR)/$@


#WINDOWS
# Alvo (target) para a construcao da biblioteca estática rodolpho.lib
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.lib: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/crash.h $(INC_DIR)/stack.h $(INC_DIR)/queue.h $(INC_DIR)/list.h $(INC_DIR)/search.h $(INC_DIR)/sort.h $(INC_DIR)/rodolpho.h
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o
	@echo "+++ [Biblioteca estática criada em $(LIB_DIR)/$@] +++"
	@echo "============="

# Alvo (target) para a construcao da biblioteca dinâmica rodolpho.a
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.dll: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/crash.h $(INC_DIR)/stack.h $(INC_DIR)/queue.h $(INC_DIR)/list.h $(INC_DIR)/search.h $(INC_DIR)/sort.h $(INC_DIR)/rodolpho.h
	$(CC) -shared -o $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o
	@echo "+++ [Biblioteca dinâmica criada em $(LIB_DIR)/$@] +++"
	@echo "============="

prog_estatico.exe:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.lib -o $(OBJ_DIR)/$@

prog_dinamico.exe:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.dll -o $(OBJ_DIR)/$@


# Alvo (target) para a geração automatica de documentacao usando o Doxygen.
# Sempre remove a documentacao anterior (caso exista) e gera uma nova.
doxy:
	@rm -rf $(DOC_DIR)/*
	doxygen Doxyfile

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos binarios/executaveis.
clean:	
	@rm -rf $(OBJ_DIR)/*