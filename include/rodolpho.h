/**
* @file 	rodolpho.h
* @brief    Arquivo cabeçalho com os includes para os arquivos com as funções que realizam busca, ordenação
*			e conteiners.
* @author   Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    26/06/2017
* @date 	27/06/2017
*/

#ifndef  RODOLPHO_H
#define  RODOLPHO_H

#include "queue.h"      /**< Arquivo com a implementação da fila */
#include "stack.h"      /**< Arquivo com a implementação da pilha */
#include "list.h"      	/**< Arquivo com a implementação da lista */
#include "search.h"     /**< Arquivo com as implementações das funções de busca */
#include "sort.h" 		/**< Arquivo com as implementações das funções de ordenação */

namespace edb1 {

}

#endif
