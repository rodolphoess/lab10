/**
* @file     queue.h
* @brief 	Arquivo com a implementação da TAD do tipo fila
* @author   Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    26/06/2017
* @date 	27/06/2017
*/

#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::cerr;
using std::string;

#include "crash.h"

/**
* @class   Fila queue.h
* @brief   Classe que representa uma fila
* @details Os atributos de uma fila são: dado, quantidade e maximo
*/
template<typename T>
class Fila {
private:
    T *dado;                           /**< Dados */
    int quantidade;                    /**< Quantidade */
    int maximo;                        /**< Máximo */
public:
    Fila(int tam);                     /**< Construtor padrão */
    ~Fila();                           /**< Destrutor padrão */
    void Push(T val);                  /**< Inserir um dado na Fila */
    T Pop();                           /**< Retirar um dado da Fila */
    bool Vazio();                      /**< Testa se a Fila está vazia */
    bool Cheio();                      /**< Testa se a Fila está cheia */
};

/**
* @details Os valores de dados e maximo são inicializados com o tamanho
*          informado e a quantidade com 0
*/
template<typename T>
Fila<T>::Fila(int tam) {
    dado = new T[tam];
    maximo = tam;
    quantidade = 0;
}

/**
* @details Destrutor padrão
*/
template<typename T>
Fila<T>::~Fila() {
    delete[] dado;
}

/**
* @details Inserir um dado na Fila
*/
template<typename T>
void Fila<T>::Push(T val) {
    if(!Cheio())
        dado[quantidade++] = val;
    else {
        //Cheia - lançar exceção
        try {
            throw FilaCheia();
        } catch (FilaCheia &ex) {
            cerr << ex.what() << endl;
        }
    }
}

/**
* @details Retirar um dado da Fila
*/
template<typename T>
T Fila<T>::Pop() {
    if(!Vazio()) {
        quantidade--;
        T retorno = dado[0];
        for(int i = 0; i < quantidade; i++)
            dado[i] = dado[i + 1];
        return retorno;
    } else  {
        try {
            throw FilaVazia();
        } catch (FilaVazia &ex) {
            cerr << ex.what() << endl;
        }
        return dado[0];
    }
}

/**
* @details Testa se a Fila está vazia
*/
template<typename T>
bool Fila<T>::Vazio() {
    return (quantidade == 0);
}

/**
* @details Testa se a Fila está cheia
*/
template<typename T>
bool Fila<T>::Cheio() {
    return (quantidade == maximo);
}

#endif