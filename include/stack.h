/**
* @file     stack.h
* @brief 	Arquivo com a implementação da TAD do tipo pilha
* @author   Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    26/06/2017
* @date 	27/06/2017
*/

#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::cerr;
using std::string;

#include "crash.h"

/**
* @class   Pilha stack.h
* @brief   Classe que representa uma pilha
* @details Os atributos de uma lista são: dado, quantidade e maximo
*/
template<typename T>
class Pilha {
private:
    T *dado;                           /**< Dado */
    int quantidade;                    /**< Quantidade */
    int maximo;                        /**< Máximo */
public:
    Pilha(int tam);                    /**< Construtor padrão */
    ~Pilha();                          /**< Destrutor padrão */
    void Push(T val);                  /**< Inserir um dado na pilha */
    T Pop();                           /**< Retirar um dado da pilha */
    bool Vazio();                      /**< Testa se a pilha está vazia */
    bool Cheio();                      /**< Testa se a pilha está cheia */
};

/**
* @details Os valores de dado e maximo são inicializados com o tamanho
*          informado e a quantidade com 0
*/
template<typename T>
Pilha<T>::Pilha(int tam) {
    dado = new T[tam];
    maximo = tam;
    quantidade = 0;
}

/**
* @details Destrutor padrão
*/
template<typename T>
Pilha<T>::~Pilha() {
    delete[] dado;
}

/**
* @details Inserir um dado na pilha
*/
template<typename T>
void Pilha<T>::Push(T val) {
    if(!Cheio())
        dado[quantidade++] = val;
    else {
        //Cheia - lançar exceção
        try {
            throw PilhaCheia();
        } catch (PilhaCheia &ex) {
            cerr << ex.what() << endl;
        }
    }
}

/**
* @details Retirar um dado da pilha
*/
template<typename T>
T Pilha<T>::Pop() {
    if(!Vazio())
        return dado[--quantidade];
    else {
        try {
            throw PilhaVazia();
        } catch (PilhaVazia &ex) {
            cerr << ex.what() << endl;
        }
        return dado[0];
    }
}

/**
* @details Testa se a pilha está vazia
*/
template<typename T>
bool Pilha<T>::Vazio() {
    return (quantidade == 0);
}

/**
* @details Testa se a pilha está cheia
*/
template<typename T>
bool Pilha<T>::Cheio() {
    return (quantidade == maximo);
}

#endif