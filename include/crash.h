/**
* @file     crash.h
* @brief 	Classes para lançamento de erros
* @author   Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    26/06/2017
* @date 	27/06/2017
*/

#ifndef CRASH_H
#define CRASH_H

#include <stdexcept>
using std::range_error;
using std::length_error;
using std::overflow_error;

/**
* @brief Erro de elemento não encontrado em uma função de busca
**/
class ElementoAusente : public range_error {
	public:
    	ElementoAusente() : range_error("O elemento requisitado não está na estrutura!") {}
};

/**
* @brief Erro de ordenação em vetor vazio
**/
class EstruturaVazia : public length_error {
	public:
    	EstruturaVazia() : length_error("A estrutura não possui elementos. Não é possível ordená-la!") {}
};

/**
* @brief Erro de pilha vazia
**/
class PilhaVazia : public range_error {
	public:
   		PilhaVazia() : range_error("A pilha está vazia!") {}
};

/**
* @brief Erro de pilha cheia
**/
class PilhaCheia : public range_error {
	public:
    	PilhaCheia() : range_error("Não pode inserir o elemento. A pilha está cheia!") {}
};

/**
* @brief Erro de fila vazia
**/
class FilaVazia : public range_error {
	public:
    	FilaVazia() : range_error("A fila está vazia!") {}
};

/**
* @brief Erro de fila cheia
**/
class FilaCheia : public range_error {
	public:
    	FilaCheia() : range_error("Não pode inserir o elemento. A fila está cheia!") {}
};

/**
* @brief Erro de lista vazia
**/
class ListaVazia : public range_error {
	public:
    	ListaVazia() : range_error("A lista está vazia!") {}
};

/**
* @brief Erro de falta de memória para alocação de objeto
**/
class MemoryOverflow : public overflow_error {
	public:
    	MemoryOverflow() : overflow_error("Não há espaço disponível em memória!") {}
};

/**
* @brief Erro de posição solicitada inacessível pelo programa
**/
class PosicaoInacessivel : public range_error {
	public:
    	PosicaoInacessivel() : range_error("Tentativa de acesso em posição inválida!") {}
};

#endif