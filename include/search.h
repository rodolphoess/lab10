/**
* @file     search.h
* @brief 	Arquivo com a implementação das funções de busca
* @author   Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    26/06/2017
* @date 	27/06/2017
*/

#ifndef SEARCH_H
#define SEARCH_H

#include "crash.h"

/**
* @brief    Função de busca linear iterativa
* @param    A Vetor com os dados
* @param    first Posição mais à esquerda no vetor para a busca
* @param    end Posição mais à direita para busca
* @param    k Elemento a ser buscado
* @return   Posição do elemento no vetor ou -1 caso não seja encontrado
*/
extern "C++" template<typename T>
int sequencialSearchI(T A[], int first, int end, T k) {
    int i, size = end - first;
    for(i = 0; i < size; i++)
        if(A[i] == k)
            return i;

     /** Elemento não encontrado - lançar exceção */
    try {
        throw ElementoAusente();
    } catch (ElementoAusente &ex) {
        cerr << ex.what() << endl;
    }
    return -1;
}

/**
* @brief   Função de busca linear recursiva
* @param   A Vetor com os dados
* @param   first Posição mais à esquerda no vetor para a busca
* @param   end Posição mais à direita para busca
* @param   k Elemento a ser buscado
* @return  Posição do elemento no vetor ou -1 caso não seja encontrado
*/
extern "C++" template<typename T>
int sequencialSearchR(T A[], int first, int end, T k) {
    int n = (end - first);
    if(n < 0) {
         /** Elemento não encontrado - lançar exceção */
        try {
            throw ElementoAusente();
        } catch (ElementoAusente &ex) {
            cerr << ex.what() << endl;
        }
        return -1;
    }
    if(A[n] == k){
        return n;
    } else
        return sequencialSearchR(A, 0, (n - 1), k);
}

/**
* @brief   Função de busca binária iterativa
* @param   A Vetor com os dados
* @param   first Posição mais à esquerda no vetor para a busca
* @param   end Posição mais à direita para busca
* @param   k Elemento a ser buscado
* @return  Posição do elemento no vetor ou -1 caso não seja encontrado
*/
extern "C++" template<typename T>
int binarySearchI(T A[], int first, int end, T k) {
    int middle;
    while(first <= end){
        middle = (first + end)/2;
        if(A[middle] == k)
            return middle;
        if(A[middle] < k)
            first = middle + 1;
        else
            end = middle - 1;
    }

    /** Elemento não encontrado - lançar exceção */
    try {
        throw ElementoAusente();
    } catch (ElementoAusente &ex) {
        cerr << ex.what() << endl;
    }
    return -1;
}

/**
* @brief    Função de busca binária recursiva
* @param    A Vetor com os dados
* @param    first Posição mais à esquerda no vetor para a busca
* @param    end Posição mais à direita para busca
* @param    k Elemento a ser buscado
* @return   Posição do elemento no vetor ou -1 caso não seja encontrado
*/
extern "C++" template<typename T>
int binarySearchR(T A[], int first, int end, T k) {
    int mid = (first + end)/2;
    if(A[mid] == k)
        return mid;
    if(first >= end) {
         /** Elemento não encontrado - lançar exceção */
        try {
            throw ElementoAusente();
        } catch (ElementoAusente &ex) {
            cerr << ex.what() << endl;
        }
        return -1;
    }
    if(A[mid] < k)
        return binarySearchR(A, (mid + 1), end, k);
    else
        return binarySearchR(A, first, (mid - 1), k);
    
    return -1;
}

/**
* @brief   Função de busca ternária iterativa
* @param   A Vetor com os dados
* @param   first Posição mais à esquerda no vetor para a busca
* @param   end Posição mais à direita para busca
* @param   k Elemento a ser buscado
* @return  Posição do elemento no vetor ou -1 caso não seja encontrado
*/
extern "C++" template<typename T>
int ternarySearchI(T A[], int first, int end, T k) {
    int mid1, mid2;
    int left = first;
    int right = end;
    do {
        mid1 = ((right - left) / 3) + left;
        mid2 = (((right - left) / 3) * 2) + left;
        if(k == A[mid1])	
            return mid1;
        if(k == A[mid2]) 
            return mid2;
        if(k < A[mid1])
            right = mid1 - 1;
        if(k > A[mid1] && k < A[mid2]){
            left = mid1 + 1;
            right = mid2 - 1;
        } else if(k > A[mid2])
            left = mid2 + 1;

    } while(left <= right);
        
    /** Elemento não encontrado - lançar exceção */
    try {
        throw ElementoAusente();
    } catch (ElementoAusente &ex) {
        cerr << ex.what() << endl;
    }
    return -1;
}

/**
* @brief    Função de busca ternária recursiva
* @param    A Vetor com os dados
* @param    first Posição mais à esquerda no vetor para a busca
* @param    end Posição mais à direita para busca
* @param    k Elemento a ser buscado
* @return   Posição do elemento no vetor ou -1 caso não seja encontrado
*/
extern "C++" template<typename T>
int ternarySearchR(T A[], int first, int end, T k) {
    if((first < 0) || (end < (end-first)-1) || (first > end)) {
         /** Elemento não encontrado - lançar exceção */
        try {
            throw ElementoAusente();
        } catch (ElementoAusente &ex) {
            cerr << ex.what() << endl;
        }
        return -1;
    }
    if(k == A[first])
        return first;
    if(k == A[end])
        return end;
    if(k < A[first])
        return ternarySearchR(A, (first - 1), end, k);
    if ((k > A[first]) && (k < A[end]))
        return ternarySearchR(A, (first + 1), (end - 1), k);
    if(k > A[end])
        return ternarySearchR(A, first, (end + 1), k);
    return -1;
}

#endif